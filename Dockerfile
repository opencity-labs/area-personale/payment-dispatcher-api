# For more information, please refer to https://aka.ms/vscode-docker-python
FROM tiangolo/uvicorn-gunicorn:python3.11

# Keeps Python from generating .pyc files in the container
ENV PYTHONDONTWRITEBYTECODE=1

# Turns off buffering for easier container logging
ENV PYTHONUNBUFFERED=1

ENV TZ=Europe/Rome

# Install pip requirements
COPY requirements.txt .
RUN python -m pip install -r requirements.txt
RUN python -m pip install pydantic[email]

# Copia i file necessari nel container
COPY . /app

# Imposta la working directory nel container
WORKDIR /app

# Esegui lo script quando il container viene avviato
CMD ["python3", "src/main.py" ]
