from prometheus_client import Counter

APP_NAME = 'payment-dispatcher-api'

API_REQUESTS_COUNT = Counter(
    'oc_api_requests_total',
    'Number of payment api responses',
    ('env', 'method', 'app_name', 'status_code'),
)


async def inc_api_metrics(env, method, status_code):
    API_REQUESTS_COUNT.labels(env, method, APP_NAME, status_code).inc()
