from unittest.mock import patch
from uuid import uuid4

import pytest
from fastapi.testclient import TestClient
from httpx import ASGITransport, AsyncClient
from oc_python_sdk.models.payment import (
    Cancel,
    Confirm,
    Debtor,
    HttpMethodType,
    Links,
    Notify,
    OfflinePayment,
    OnlinePaymentBegin,
    OnlinePaymentLanding,
    Payer,
    PaymentData,
    Receipt,
    Update,
)

from src.main import Payment, app, get_payment_v1

client = TestClient(app)


# Helpers to create valid instances
def create_valid_payment_payload(include_optional_fields=True):
    payload = {
        'config_id': str(uuid4()),
        'tenant_id': str(uuid4()),
        'user_id': str(uuid4()),
        'remote_collection': {
            'id': str(uuid4()),
            'type': 'collection_type',
        },
        'payment': {
            'type': 'PAGOPA',
            'expire_at': '2024-12-31T23:59:59Z',
            'amount': 100.0,
            'currency': 'EUR',
            'notice_code': '123456789012345678',
            'iuv': '12345678901',
            'reason': 'Payment for service',
            'receiver': {
                'tax_identification_number': 'TAX98765432',
                'name': 'Comune di Bugliano',
            },
            'document': {
                'id': '1',
                'ref': 'ref1',
                'hash': 'sha256:abcdef1234567890',
            },
            'split': [
                {'code': 'split1', 'amount': 50.0, 'meta': {}},
                {'code': 'split2', 'amount': 50.0, 'meta': {}},
            ],
        },
        'links': {
            'online_payment_begin': {
                'method': 'GET',
                'url': 'https://example.com/begin',
            },
            'online_payment_landing': {
                'method': 'POST',
                'url': 'https://example.com/landing',
            },
            'notify': [
                {'method': 'POST', 'url': 'https://example.com/notify'},
            ],
            'offline_payment': {
                'method': 'GET',
                'url': 'https://example.com/offline',
            },
            'receipt': {
                'method': 'GET',
                'url': 'https://example.com/receipt',
            },
        },
        'debtor': {
            'type': 'human',
            'tax_identification_number': 'TAX12345678',
            'name': 'John',
            'family_name': 'Doe',
            'street_name': 'Main St',
            'building_number': '1',
            'postal_code': '12345',
            'town_name': 'Townsville',
            'country_subdivision': 'Region',
            'country': 'IT',
            'email': 'john.doe@example.com',
        },
        'is_external': False,
    }

    if include_optional_fields:
        payload['links'].update(
            {
                'update': {
                    'method': 'PATCH',
                    'url': 'https://example.com/update',
                },
                'confirm': {
                    'method': 'POST',
                    'url': 'https://example.com/confirm',
                },
                'cancel': {
                    'method': 'DELETE',
                    'url': 'https://example.com/cancel',
                },
            },
        )

    return payload


def test_status_endpoint():
    response = client.get('/status')
    assert response.status_code == 200
    assert response.json() == {'status': 'Everything OK!'}


@pytest.mark.asyncio
@patch('src.main.send_event')
async def test_create_payment_endpoint_with_optional_fields(mock_send_event):
    payload = create_valid_payment_payload()
    async with AsyncClient(transport=ASGITransport(app=app), base_url='http://test') as ac:
        response = await ac.post('/payment', json=payload)
    assert response.status_code == 200
    response_json = response.json()
    assert 'id' in response_json
    assert response_json['status'] == 'CREATION_PENDING'


@pytest.mark.asyncio
@patch('src.main.send_event')
async def test_create_payment_endpoint_without_optional_fields(mock_send_event):
    payload = create_valid_payment_payload(include_optional_fields=False)
    async with AsyncClient(transport=ASGITransport(app=app), base_url='http://test') as ac:
        response = await ac.post('/payment', json=payload)
    assert response.status_code == 200
    response_json = response.json()
    assert 'id' in response_json
    assert response_json['status'] == 'CREATION_PENDING'


@pytest.mark.asyncio
async def test_create_payment_validation_error():
    invalid_payload = create_valid_payment_payload()
    invalid_payload['config_id'] = 'invalid-uuid'
    async with AsyncClient(transport=ASGITransport(app=app), base_url='http://test') as ac:
        response = await ac.post('/payment', json=invalid_payload)
    assert response.status_code == 422


@pytest.mark.asyncio
@patch('src.main.send_event', side_effect=Exception('Mocked Kafka Error'))
async def test_create_payment_internal_error(mock_send_event):
    payload = create_valid_payment_payload()
    async with AsyncClient(transport=ASGITransport(app=app), base_url='http://test') as ac:
        response = await ac.post('/payment', json=payload)
    assert response.status_code == 500
    response_json = response.json()
    assert response_json['title'] == 'Internal Server Error'
    mock_send_event.assert_awaited_once()


def test_get_payment_v1():
    # Create a dummy Payment instance with all required fields
    payment = Payment(
        id=uuid4(),
        user_id=str(uuid4()),
        type='PAGOPA',
        tenant_id=str(uuid4()),
        service_id=str(uuid4()),
        created_at='2024-01-01T00:00:00Z',
        updated_at='2024-01-01T01:00:00Z',
        status='CREATION_PENDING',
        remote_id=str(uuid4()),
        reason='Reason for payment',
        payment=PaymentData(
            transaction_id=None,
            paid_at=None,
            expire_at='2024-12-31T23:59:59Z',
            amount=100.0,
            currency='EUR',
            type=None,
            notice_code='123456789012345678',
            iud='1234567890abcdef1234567890abcdef',
            iuv='12345678901',
            receiver=None,
            due_type=None,
            pagopa_category=None,
            document={
                'id': '1',
                'ref': 'ref1',
                'hash': 'sha256:abcdef1234567890',
            },
            split=[
                {'code': 'split1', 'amount': 50.0, 'meta': {}},
                {'code': 'split2', 'amount': 50.0, 'meta': {}},
            ],
        ),
        links=Links(
            online_payment_begin=OnlinePaymentBegin(
                url='https://example.com/begin',
                last_opened_at=None,
                method='GET',
            ),
            online_payment_landing=OnlinePaymentLanding(
                url='https://example.com/landing',
                last_opened_at=None,
                method='POST',
            ),
            offline_payment=OfflinePayment(
                url='https://example.com/offline',
                last_opened_at=None,
                method='GET',
            ),
            receipt=Receipt(
                url='https://example.com/receipt',
                last_opened_at=None,
                method='GET',
            ),
            notify=[
                Notify(
                    url=None,
                    method=None,
                    sent_at=None,
                ),
            ],
            update=Update(
                url=None,
                last_check_at=None,
                next_check_at=None,
                method=HttpMethodType.HTTP_METHOD_GET,
            ),
            confirm=Confirm(
                url=None,
                last_opened_at=None,
                method=HttpMethodType.HTTP_METHOD_PATCH,
            ),
            cancel=Cancel(
                url=None,
                last_opened_at=None,
                method=HttpMethodType.HTTP_METHOD_PATCH,
            ),
        ),
        payer=Payer(
            type='human',
            tax_identification_number='TAX12345678',
            name='John Doe',
            family_name='Doe',
            street_name='Main St',
            building_number='1',
            postal_code='12345',
            town_name='Townsville',
            country_subdivision='Region',
            country='IT',
            email='john.doe@example.com',
        ),
        debtor=Debtor(
            type='human',
            tax_identification_number='TAX12345678',
            name='John Doe',
            family_name='Doe',
            street_name='Main St',
            building_number='1',
            postal_code='12345',
            town_name='Townsville',
            country_subdivision='Region',
            country='IT',
            email='john.doe@example.com',
        ),
        locale='it',
        event_id=uuid4(),
        event_version='2.0',
        event_created_at='2024-01-01T00:00:00Z',
        app_id='payment-dispatcher-api:1.0.0',
    )

    # Test the function
    payment_v1 = get_payment_v1(payment)

    assert 'id' in payment_v1
    assert payment_v1['event_version'] == '1.0'
    assert payment_v1['payment']['iud'] == payment_v1['id'].hex
    assert isinstance(payment_v1['payment']['split'], dict)
