from enum import Enum
from typing import List, Optional
from uuid import UUID

from dateutil import parser
from pydantic import (
    BaseModel,
    EmailStr,
    Field,
    HttpUrl,
    ValidationError,
    root_validator,
    validator,
)


def check_iso_format(value: Optional[str]):
    try:
        if value:
            parser.parse(value)
        return value
    except Exception:
        raise ValidationError('date must be in ISO8601 format')


class Document(BaseModel):
    id: Optional[str]
    ref: Optional[str]
    hash: str = Field(..., example='sha256:abcdef1234567890')


class Split(BaseModel):
    code: str = Field(..., example='split_code')
    amount: float = Field(..., ge=0.00001, description='Amount in double precision')

    class Config:
        schema_extra = {
            'properties': {
                'amount': {'type': 'number', 'format': 'double'},
            },
        }


class Receiver(BaseModel):
    tax_identification_number: str = Field(..., min_length=1, description='Tax ID is required')
    name: str = Field(..., min_length=1, description='Name is required')


class Payment(BaseModel):
    expire_at: Optional[str]
    amount: Optional[float] = Field(
        None,
        ge=0.00001,
        description='Amount in double precision',
    )
    currency: Optional[str]
    notice_code: Optional[str]
    iuv: Optional[str]
    reason: Optional[str]
    receiver: Optional[Receiver]
    document: Optional[Document]
    split: Optional[List[Split]] = Field(default_factory=list)

    _expire_at_must_be_iso8601 = validator('expire_at', allow_reuse=True)(
        check_iso_format,
    )

    class Config:
        schema_extra = {
            'properties': {
                'amount': {'type': 'number', 'format': 'double'},
                'split': {
                    'type': 'array',
                    'items': {
                        'type': 'object',
                        'properties': {
                            'code': {
                                'type': 'string',
                                'title': 'Code',
                                'example': 'split_code',
                            },
                            'amount': {'type': 'number', 'format': 'double'},
                        },
                    },
                },
            },
        }


class Link(BaseModel):
    method: str
    url: HttpUrl


class Notify(Link):
    pass


class Links(BaseModel):
    online_payment_begin: Optional[Link]
    online_payment_landing: Link
    notify: List[Optional[Notify]]
    offline_payment: Optional[Link]
    receipt: Optional[Link]
    update: Optional[Link]
    confirm: Optional[Link]
    cancel: Optional[Link]


class PayerType(str, Enum):
    TYPE_HUMAN = 'human'
    TYPE_LEGAL = 'legal'


class Debtor(BaseModel):
    type: PayerType
    tax_identification_number: str = Field(..., example='TAX12345678')
    name: Optional[str] = ''
    family_name: Optional[str]
    street_name: Optional[str]
    building_number: Optional[str] = ''
    postal_code: Optional[str]
    town_name: Optional[str]
    country_subdivision: Optional[str]
    country: Optional[str]
    email: Optional[EmailStr]


class RemoteCollection(BaseModel):
    id: UUID
    type: str


class PaymentPayload(BaseModel):
    config_id: UUID
    tenant_id: UUID
    user_id: UUID
    remote_collection: RemoteCollection
    payment: Payment
    links: Links
    debtor: Debtor
    created_at: Optional[str]
    is_external: bool

    _created_at_must_be_iso8601 = validator('created_at', allow_reuse=True)(
        check_iso_format,
    )

    @root_validator()
    @classmethod
    def validate_all_fields(cls, values):
        payment = values.get('payment')
        links = values.get('links')

        if not payment:
            raise ValueError('Payment information is required.')

        if not links:
            raise ValueError('Links information is required.')

        if values.get('is_external'):
            if not payment.notice_code and not payment.iuv:
                raise ValueError(
                    'If payment is external, either notice_code or iuv must be provided',
                )
            required_links = [
                'online_payment_begin',
                'offline_payment',
                'receipt',
            ]
            for link in required_links:
                if not getattr(links, link):
                    raise ValueError(
                        f'If payment is external, {link} link must be provided',
                    )

        return values
