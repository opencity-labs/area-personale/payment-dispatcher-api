import asyncio
import json
import os
import signal
import uuid
from contextlib import asynccontextmanager
from datetime import datetime
from typing import cast
from urllib.request import Request

import sentry_sdk
import uvicorn
from aiokafka import AIOKafkaProducer
from dotenv import load_dotenv
from fastapi import FastAPI
from fastapi.exceptions import RequestValidationError
from fastapi.openapi.utils import get_openapi
from fastapi.responses import JSONResponse
from oc_python_sdk.models.payment import (
    Cancel,
    Confirm,
    CurrencyType,
    Debtor,
    HttpMethodType,
    Links,
    Notify,
    OfflinePayment,
    OnlinePaymentBegin,
    OnlinePaymentLanding,
    Payer,
    PayerType,
    Payment,
    PaymentData,
    PaymentDataSplit,
    PaymentStatus,
    Receipt,
    Update,
)
from pydantic import BaseModel, ValidationError
from starlette_exporter import PrometheusMiddleware, handle_metrics

from src.__init__ import __version__ as app_version
from src.errors import ERR_422_SCHEMA, ErrorMessage, JSONResponseCustom, error_message
from src.kafka_producer import send_event
from src.logger import proxy_logger as logger
from src.models import PaymentPayload
from src.monitoring import inc_api_metrics

load_dotenv()

logger.info('Starting')
app_name = 'payment-dispatcher-api'
APP_HOST = os.environ.get('APP_HOST', '0.0.0.0')
APP_PORT = os.environ.get('APP_PORT', 8001)
EXTERNAL_API_URL = os.environ.get('EXTERNAL_API_URL', 'http://0.0.0.0:8001')
PROMETHEUS_PREFIX = os.getenv('PROMETHEUS_PREFIX', default='payment_dispatcher_api')
KAFKA_SERVERS = os.getenv('KAFKA_SERVERS').split(',')
FASTAPI_BASE_URL = os.getenv('FASTAPI_BASE_URL', '')
ENV = os.getenv('ENV', 'LOCAL')
SENTRY_ENABLED = os.getenv('SENTRY_ENABLED')
SENTRY_DSN = os.getenv('SENTRY_DSN')
APP_EVENT_VERSION = '2.0'
COUNTRY_CODE = 'IT'


@asynccontextmanager
async def lifespan(app: FastAPI):
    """Gestisce le risorse globali, incluso Kafka"""
    logger.info('Application startup: initializing resources...')

    # Inizializza il Kafka Producer UNA SOLA VOLTA e assegna a `state`
    producer = AIOKafkaProducer(bootstrap_servers=KAFKA_SERVERS)
    await producer.start()

    # Evita il warning su `state`
    app.state.kafka_producer = cast(AIOKafkaProducer, producer)

    yield  # ⬅️ Mantiene l'app attiva

    # Shutdown: chiudi il producer di Kafka
    logger.info('Application shutdown: releasing Kafka producer...')
    await app.state.kafka_producer.stop()


app = FastAPI(
    lifespan=lifespan,
    openapi_url=f'{FASTAPI_BASE_URL}/openapi.json',
    docs_url=f'{FASTAPI_BASE_URL}/docs',
    openapi_tags=[{'name': 'Status'}, {'name': 'Payments'}],
    responses={422: ERR_422_SCHEMA},
)

if SENTRY_ENABLED:
    sentry_sdk.init(
        dsn=SENTRY_DSN,
        # Set traces_sample_rate to 1.0 to capture 100%
        # of transactions for performance monitoring.
        traces_sample_rate=1.0,
        # Set profiles_sample_rate to 1.0 to profile 100%
        # of sampled transactions.
        # We recommend adjusting this value in production.
        profiles_sample_rate=1.0,
        environment=ENV,
    )


app.add_middleware(
    PrometheusMiddleware,
    app_name=app_name,
    prefix=PROMETHEUS_PREFIX,
    group_paths=False,  # Mantiene il comportamento attuale
    filter_unhandled_paths=False,  # Mantiene il comportamento attuale
)
app.add_route('/metrics', handle_metrics)


class Status(BaseModel):
    status: str


@app.exception_handler(RequestValidationError)
async def validation_exception_handler(request: Request, exc: RequestValidationError):
    await inc_api_metrics(env=ENV, method=request.method, status_code=422)
    return error_message(
        err_type='/errors/request-validation-error',
        title='Request Validation Error',
        status=422,
        detail=exc.__dict__,
        instance=None,
    )


@app.exception_handler(ValidationError)
async def validation_error_handler(request: Request, exc: ValidationError):
    await inc_api_metrics(env=ENV, method=request.method, status_code=422)
    return error_message(
        err_type='/errors/validation-error',
        title='Validation Error',
        status=422,
        detail=exc.__dict__,
        instance=None,
    )


@app.exception_handler(Exception)
async def general_exception_handler(request: Request, exc: Exception):
    await inc_api_metrics(env=ENV, method=request.method, status_code=500)
    return error_message(
        err_type='/errors/internal-server-error',
        title='Internal Server Error',
        status=500,
        detail=exc.__dict__,
        instance=None,
    )


@app.get(
    '/status',
    tags=['Status'],
    description='Get Service Status',
    status_code=200,
    response_class=JSONResponseCustom,
    responses={
        503: {'model': ErrorMessage, 'description': 'Service Unavailable'},
        200: {'model': Status, 'description': 'Successful Response'},
    },
)
async def get_status() -> JSONResponse:
    try:
        await inc_api_metrics(env=ENV, method='GET', status_code=200)
        return JSONResponse(
            {'status': 'Everything OK!'},
            media_type='application/problem+json',
        )
    except Exception as exc:
        await inc_api_metrics(env=ENV, method='GET', status_code=503)
        return error_message(
            err_type='/errors/service-unavailable',
            title='Service Unavailable',
            status=503,
            detail=str(exc),
            instance=None,
        )


@app.post('/payment', tags=['Payments'])
async def create_payment(payload: PaymentPayload):
    try:
        logger.debug(f'Creating payment with payload {payload}')
        payment_v2 = get_payment_v2(payload)
        payment_v1 = get_payment_v1(payment_v2)

        # Send the event to Kafka
        await send_event(app, json.loads(payment_v2.json()))
        await send_event(app, payment_v1)

        logger.info(
            f'Created payment {payment_v2.id} with version {payment_v2.event_version} for '
            f'{payload.remote_collection.type} {payload.remote_collection.id}',
        )
        logger.info(
            f'Created payment {payment_v1["id"]} with version {payment_v1["event_version"]} for '
            f'{payload.remote_collection.type} {payload.remote_collection.id}',
        )

        return payment_v2
    except ValidationError as e:
        logger.error(f'A validation error occurred: {e}')
        await inc_api_metrics(env=ENV, method='POST', status_code=422)
        return error_message(
            err_type='/errors/validation-error',
            title='Validation Error',
            status=422,
            detail=str(e),
            instance=None,
        )
    except Exception as e:
        logger.error(f'An error occurred: {e}')
        await inc_api_metrics(env=ENV, method='POST', status_code=500)
        return error_message(
            err_type='/errors/internal-server-error',
            title='Internal Server Error',
            status=500,
            detail=str(e),
            instance=None,
        )


def get_payment_v2(payload: PaymentPayload):
    payment_id = uuid.uuid4()
    return Payment(
        id=payment_id,
        user_id=payload.user_id,
        type='PAGOPA',
        tenant_id=payload.tenant_id,
        service_id=payload.config_id,
        created_at=payload.created_at
        or datetime.now().replace(microsecond=0).astimezone().isoformat(),
        updated_at=datetime.now().replace(microsecond=0).astimezone().isoformat(),
        status=PaymentStatus.STATUS_CREATION_PENDING
        if not payload.is_external
        else PaymentStatus.STATUS_PAYMENT_PENDING,
        remote_id=payload.remote_collection.id,
        reason=payload.payment.reason,
        payment=PaymentData(
            transaction_id=None,
            paid_at=None,
            expire_at=str(payload.payment.expire_at)
            if payload.payment.expire_at
            else None,
            amount=payload.payment.amount,
            currency=CurrencyType(payload.payment.currency)
            if payload.payment.currency
            else CurrencyType('EUR'),
            type=None,
            notice_code=payload.payment.notice_code,
            iud=payment_id.hex,
            iuv=payload.payment.iuv,
            receiver=payload.payment.receiver,
            due_type=None,
            pagopa_category=None,
            document=payload.payment.document,
            split=[
                PaymentDataSplit(code=item.code, amount=item.amount, meta={})
                for item in payload.payment.split
            ],
        ),
        links=Links(
            online_payment_begin=OnlinePaymentBegin(
                url=None,
                last_opened_at=None,
                method=HttpMethodType.HTTP_METHOD_GET,
            )
            if not payload.is_external
            else OnlinePaymentBegin(
                url=f'{payload.links.online_payment_begin.url}/{payment_id}',
                last_opened_at=None,
                method=HttpMethodType(payload.links.online_payment_begin.method),
            ),
            online_payment_landing=OnlinePaymentLanding(
                url=f'{payload.links.online_payment_landing.url}',
                last_opened_at=None,
                method=HttpMethodType(payload.links.online_payment_landing.method),
            ),
            offline_payment=OfflinePayment(
                url=f'{payload.links.offline_payment.url}/{payment_id}',
                last_opened_at=None,
                method=HttpMethodType(payload.links.offline_payment.method),
            )
            if payload.is_external
            else OfflinePayment(
                url=None,
                last_opened_at=None,
                method=HttpMethodType.HTTP_METHOD_GET,
            ),
            receipt=Receipt(
                url=f'{payload.links.receipt.url}/{payment_id}',
                last_opened_at=None,
                method=HttpMethodType(payload.links.receipt.method),
            )
            if payload.is_external
            else Receipt(
                url=None,
                last_opened_at=None,
                method=HttpMethodType.HTTP_METHOD_GET,
            ),
            notify=[
                Notify(
                    url=item.url,
                    method=HttpMethodType(item.method),
                    sent_at=None,
                )
                for item in payload.links.notify
            ],
            update=Update(
                url=None,
                last_check_at=None,
                next_check_at=None,
                method=HttpMethodType.HTTP_METHOD_GET,
            ),
            confirm=Confirm(
                url=None,
                last_opened_at=None,
                method=HttpMethodType.HTTP_METHOD_PATCH,
            ),
            cancel=Cancel(
                url=None,
                last_opened_at=None,
                method=HttpMethodType.HTTP_METHOD_PATCH,
            ),
        ),
        payer=Payer(
            type=PayerType.TYPE_HUMAN,
            tax_identification_number=payload.debtor.tax_identification_number,
            name=payload.debtor.name,
            family_name=payload.debtor.family_name,
            street_name=payload.debtor.street_name,
            building_number=payload.debtor.building_number,
            postal_code=payload.debtor.postal_code,
            town_name=payload.debtor.town_name,
            country_subdivision=payload.debtor.country_subdivision,
            country=COUNTRY_CODE,
            email=payload.debtor.email,
        ),
        debtor=Debtor(
            type=PayerType.TYPE_HUMAN,
            tax_identification_number=payload.debtor.tax_identification_number,
            name=payload.debtor.name,
            family_name=payload.debtor.family_name,
            street_name=payload.debtor.street_name,
            building_number=payload.debtor.building_number,
            postal_code=payload.debtor.postal_code,
            town_name=payload.debtor.town_name,
            country_subdivision=payload.debtor.country_subdivision,
            country=COUNTRY_CODE,
            email=payload.debtor.email,
        ),
        locale='it',
        event_id=uuid.uuid4(),
        event_version=APP_EVENT_VERSION,
        event_created_at=datetime.now().replace(microsecond=0).astimezone().isoformat(),
        app_id=f'{app_name}:{app_version}',
    )


def get_payment_v1(payment: Payment):
    payment = json.loads(payment.json())
    payment_id = uuid.uuid4()
    payment['id'] = payment_id
    payment['payment']['iud'] = payment_id.hex
    payment['payment']['split'] = {
        item['code']: item['amount'] for item in payment['payment']['split']
    } or payment['payment']['split']
    payment['event_version'] = '1.0'
    return payment


def custom_openapi():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title='Payment Dispatcher API',
        version=app_version,
        tags=[{'name': 'Status'}, {'name': 'Payments'}],
        servers=[
            {
                'url': EXTERNAL_API_URL,
                'description': 'payment-dispatcher-api',
                'x-sandbox': True,
            },
        ],
        description='Questo servizio si occupa di creare pagamenti via API per Opencity - Area Personale',
        contact={
            'name': 'Opencity Labs Srl',
            'url': 'https://opencitylabs.it/collabora-con-noi/',
            'email': 'info@opencitylabs.it',
        },
        terms_of_service='http://example.com/terms/',
        routes=app.routes,
    )
    openapi_schema['info']['x-api-id'] = 'payment-dispatcher-api'
    openapi_schema['info']['x-summary'] = (
        'Questo servizio si occupa di creare pagamenti via API per Opencity - Area Personale',
    )
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = custom_openapi


def run_server():
    """Esegue il server intercettando SIGINT e SIGTERM per uno shutdown pulito"""
    config = uvicorn.Config(
        app,
        host=APP_HOST,
        port=int(APP_PORT),
        proxy_headers=True,
        forwarded_allow_ips='*',
        access_log=False,
    )
    server = uvicorn.Server(config)

    loop = asyncio.get_event_loop()

    # Intercetta SIGINT (CTRL+C) e SIGTERM (Docker/Kubernetes)
    for sig in (signal.SIGINT, signal.SIGTERM):
        loop.add_signal_handler(sig, lambda: asyncio.create_task(server.shutdown()))

    try:
        loop.run_until_complete(server.serve())
    except asyncio.CancelledError:
        pass  # Evita traceback in console


if __name__ == '__main__':
    run_server()
