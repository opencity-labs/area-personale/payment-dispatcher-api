from uuid import uuid4

import pytest
from pydantic import ValidationError

from src.models import (
    Debtor,
    Document,
    Link,
    Links,
    Notify,
    PayerType,
    Payment,
    PaymentPayload,
    Receiver,
    RemoteCollection,
    Split,
)


# Helpers to create valid instances
def create_valid_payment(receiver=None):
    return Payment(
        expire_at='2024-12-31T23:59:59Z',
        amount=100.0,
        currency='EUR',
        notice_code='123456789012345678',
        iuv='12345678901',
        reason='Payment for service',
        receiver=receiver,
        document=Document(id='1', ref='ref1', hash='sha256:abcdef1234567890'),
        split=[Split(code='split1', amount=50.0), Split(code='split2', amount=50.0)],
    )


def create_valid_links():
    return Links(
        online_payment_begin=Link(method='GET', url='https://example.com/begin'),
        online_payment_landing=Link(method='POST', url='https://example.com/landing'),
        notify=[Notify(method='POST', url='https://example.com/notify')],
        offline_payment=Link(method='GET', url='https://example.com/offline'),
        receipt=Link(method='GET', url='https://example.com/receipt'),
        update=Link(method='PATCH', url='https://example.com/update'),
        confirm=Link(method='POST', url='https://example.com/confirm'),
        cancel=Link(method='DELETE', url='https://example.com/cancel'),
    )


def create_valid_debtor():
    return Debtor(
        type=PayerType.TYPE_HUMAN,
        tax_identification_number='TAX12345678',
        name='John',
        family_name='Doe',
        street_name='Main St',
        building_number='1',
        postal_code='12345',
        town_name='Townsville',
        country_subdivision='Region',
        country='IT',
        email='john.doe@example.com',
    )


def create_valid_remote_collection():
    return RemoteCollection(id=uuid4(), type='collection_type')


def create_valid_payment_payload(payment=None, is_external=False):
    return PaymentPayload(
        config_id=uuid4(),
        tenant_id=uuid4(),
        user_id=uuid4(),
        remote_collection=create_valid_remote_collection(),
        payment=payment or create_valid_payment(),
        links=create_valid_links(),
        debtor=create_valid_debtor(),
        is_external=is_external,
    )


# Test cases
def test_valid_payment_payload_without_receiver():
    payload = create_valid_payment_payload()
    assert payload.payment.receiver is None
    assert payload.payment.amount == 100.0
    assert payload.payment.currency == 'EUR'
    assert payload.debtor.email == 'john.doe@example.com'


def test_valid_payment_payload_with_valid_receiver():
    receiver = Receiver(tax_identification_number='TAX98765432', name='Acme Corp')
    payment = create_valid_payment(receiver=receiver)
    payload = create_valid_payment_payload(payment=payment)
    assert payload.payment.receiver.tax_identification_number == 'TAX98765432'
    assert payload.payment.receiver.name == 'Acme Corp'


def test_invalid_payment_payload_with_invalid_receiver():
    with pytest.raises(ValidationError, match='ensure this value has at least 1 characters'):
        Receiver(tax_identification_number='', name='')


def test_invalid_iso_date():
    with pytest.raises(ValidationError):
        Payment(expire_at='not-a-date')


def test_invalid_split_amount():
    with pytest.raises(ValidationError):
        Split(code='split', amount=-10.0)


def test_external_payment_missing_notice_code_and_iuv():
    payment = create_valid_payment()
    payment.notice_code = None
    payment.iuv = None
    with pytest.raises(
        ValueError,
        match='If payment is external, either notice_code or iuv must be provided',
    ):
        create_valid_payment_payload(payment=payment, is_external=True)


def test_external_payment_missing_links():
    links = create_valid_links()
    links.online_payment_begin = None
    with pytest.raises(
        ValueError,
        match='If payment is external, online_payment_begin link must be provided',
    ):
        PaymentPayload(
            config_id=uuid4(),
            tenant_id=uuid4(),
            user_id=uuid4(),
            remote_collection=create_valid_remote_collection(),
            payment=create_valid_payment(),
            links=links,
            debtor=create_valid_debtor(),
            is_external=True,
        )


def test_missing_payment():
    with pytest.raises(ValueError, match='Payment information is required.'):
        PaymentPayload(
            config_id=uuid4(),
            tenant_id=uuid4(),
            user_id=uuid4(),
            remote_collection=create_valid_remote_collection(),
            links=create_valid_links(),
            debtor=create_valid_debtor(),
            is_external=True,
        )


def test_missing_links():
    with pytest.raises(ValueError, match='Links information is required.'):
        PaymentPayload(
            config_id=uuid4(),
            tenant_id=uuid4(),
            user_id=uuid4(),
            remote_collection=create_valid_remote_collection(),
            payment=create_valid_payment(),
            debtor=create_valid_debtor(),
            is_external=True,
        )
