import json
import os

from dotenv import load_dotenv
from fastapi import FastAPI
from oc_python_sdk.models.payment import PaymentEncoder

from src.logger import kafka_logger as logger

load_dotenv()

KAFKA_TOPIC = os.getenv('KAFKA_TOPIC')


async def send_event(app: FastAPI, event: dict):
    """Invia un evento Kafka usando il producer esistente"""
    producer = app.state.kafka_producer  # Usa il producer inizializzato in lifespan()
    try:
        logger.debug(f'Producing event {event}')
        value = json.dumps(event, cls=PaymentEncoder).encode('utf-8')
        await producer.send_and_wait(
            topic=KAFKA_TOPIC,
            value=value,
            key=event['service_id'].encode('utf-8'),
        )
    except Exception as e:
        logger.error(f'Error producing Kafka event: {e}')
